#!/bin/bash

PACKAGE_DIRECTORY="/data/packages"
DISTRIBUTIONS="focal jammy bullseye"
ARCHS="amd64"

# Let's generate the repository for each distribution
for cur_distrib in ${DISTRIBUTIONS}
do
	# Now for specific architecture
	for cur_arch in ${ARCHS}
	do
		echo ${cur_distrib}-${cur_arch}

		# First create all the needed directories
		mkdir -p ${PACKAGE_DIRECTORY}/${cur_distrib}/{dists/${cur_distrib},pool}/main/binary-${cur_arch}

		# Generate the packages file
		cd ${PACKAGE_DIRECTORY}/${cur_distrib}/
		apt-ftparchive packages pool/main/binary-${cur_arch} > dists/${cur_distrib}/main/binary-${cur_arch}/Packages

		# Remove the previous xz file
		if [ -f dists/${cur_distrib}/main/binary-${cur_arch}/Packages.xz ];
		then
			rm -f dists/${cur_distrib}/main/binary-${cur_arch}/Packages.xz
		fi

		# Generate the Packages.xz file
		xz -k --quiet dists/${cur_distrib}/main/binary-${cur_arch}/Packages

		# Generate the release file
		cat > /tmp/release.conf <<BLOCK
APT::FTPArchive::Release::Codename "${cur_distrib}";
APT::FTPArchive::Release::suite "${cur_distrib}";
APT::FTPArchive::Release::Components main;
APT::FTPArchive::Release::Label "FlOpEDT repository for ${cur_distrib} distribution";
APT::FTPArchive::Release::Architectures "${cur_arch}";
BLOCK
	done

		# Purge Release files before generating in order to avoid old Release files to be included in the Release file hash list
		rm -Rf dists/${cur_distrib}/Release*
		apt-ftparchive release -c /tmp/release.conf dists/${cur_distrib} > dists/${cur_distrib}/temp_release && mv dists/${cur_distrib}/temp_release dists/${cur_distrib}/Release

		# Sign the release file
		gpg --yes -abs -o ${PACKAGE_DIRECTORY}/${cur_distrib}/dists/${cur_distrib}/Release.gpg ${PACKAGE_DIRECTORY}/${cur_distrib}/dists/${cur_distrib}/Release
done
